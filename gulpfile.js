var gulp       = require('gulp'),
  sass         = require('gulp-sass'),
  concat       = require('gulp-concat'),
  uglify       = require('gulp-uglifyjs'),
  autoprefixer = require('gulp-autoprefixer'),
  cssnano      = require('gulp-cssnano'),
  rename       = require('gulp-rename'),
  imagemin     = require('gulp-imagemin'),
  cache        = require('gulp-cache'),
  include      = require('gulp-html-partial'),
  browserSync  = require('browser-sync'),
  pngquant     = require('imagemin-pngquant'),
  del          = require('del');

gulp.task('sass', function() {
  return gulp.src('public/sass/*.sass')
 .pipe(sass().on('error', sass.logError))
 .pipe(autoprefixer(['last 15 versions']))
 .pipe(gulp.dest('public/css'))
 .pipe(browserSync.reload({stream: true}));
});

gulp.task('scripts', function(){
  return gulp.src([
    'public/lib/js/jquery.min.js',
    'public/lib/js/owl.carousel.min.js',
  ])
  .pipe(concat('libs.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('public/js'));
});

gulp.task('css-libs', ['sass'], function(){
  return gulp.src('public/css/libs.css')
  .pipe(cssnano())
  .pipe(rename({'suffix' : '.min'}))
  .pipe(gulp.dest('public/css'));
});

gulp.task('clean', function(){
  return del.sync('build');
});

gulp.task('clear', function(){
  return cache.clearAll();
});

gulp.task('img', function(){
  return gulp.src('public/img/**/*')
    .pipe(cache(imagemin({
      interlaced: true,
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    })))
    .pipe(gulp.dest('build/img'));
});

gulp.task('html', function () {
    gulp.src(['src/*.html'])
        .pipe(htmlPartial({
            basePath: 'src/partials/'
        }))
        .pipe(gulp.dest('build'));
});

gulp.task('watch', ['browser-sync', 'css-libs', 'scripts'], function() {
  gulp.watch('public/sass/**/*.sass', ['sass']);
  gulp.watch('public/*.html', browserSync.reload);
  gulp.watch('public/js/**/*.js', browserSync.reload);
});

gulp.task('browser-sync', function(){
  browserSync({
    server: {
      baseDir: 'public'
    },
    notify: false
  });
});

gulp.task('build', ['clean', 'sass', 'scripts', 'img'], function(){
  var buildCss = gulp.src([
    'public/css/main.css',
    'public/css/libs.min.css',
  ]).pipe(gulp.dest('build/css'));

  var buildFonts = gulp.src('public/fonts/**/*')
    .pipe(gulp.dest('build/fonts'));

  var buildJs = gulp.src('public/js/**/*')
    .pipe(gulp.dest('build/js'));

  var buildHtml = gulp.src('public/*.html')
    .pipe(gulp.dest('build'));
});

gulp.task('default', ['watch']);